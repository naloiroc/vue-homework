import Delay from "./delay.js";
const queryData = async () => {
  await Delay(1);
  return [
    {
      EmpNo: "AB1234",
      Name: "OOO",
      Tel: 18,
    },
    {
      EmpNo: "EQ0847",
      Name: "XXX",
      Tel: 18,
    },
  ];
};

export { queryData };
